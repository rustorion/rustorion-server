#[test]
fn basic_calls() {
	let mut server = std::process::Command::new("./target/debug/rustorion-server").arg("localhost:4433").arg("--universe").arg("small-universe.cbor").arg("s.der").arg("sk.der").spawn().expect("Failed to spawn server");
	std::thread::sleep(std::time::Duration::from_secs(1));
	
	let mut client = std::process::Command::new("./target/debug/rustorion-testclient").arg("localhost:4433").arg("c.der").arg("ck.der").spawn().expect("Failed to spawn client");
	std::thread::sleep(std::time::Duration::from_secs(1));
	client.kill().unwrap();
	
	server.kill().unwrap();
	assert_eq!(client.wait().unwrap().success(), true);
	server.wait().unwrap();
}

#[test]
fn limited_slots() {

	let mut server = std::process::Command::new("./target/debug/rustorion-server").arg("localhost:4434").arg("--universe").arg("small-universe.cbor").arg("s.der").arg("sk.der").spawn().expect("Failed to spawn server");
	std::thread::sleep(std::time::Duration::from_secs(1));
	
	let mut client1 = std::process::Command::new("./target/debug/rustorion-testclient").arg("localhost:4434").arg("c.der").arg("ck.der").spawn().expect("Failed to spawn client");
	let mut client2 = std::process::Command::new("./target/debug/rustorion-testclient").arg("localhost:4434").arg("c1.der").arg("ck1.der").spawn().expect("Failed to spawn client");
	std::thread::sleep(std::time::Duration::from_secs(1));
	let mut client3 = std::process::Command::new("./target/debug/rustorion-testclient").arg("localhost:4434").arg("c2.der").arg("ck2.der").spawn().expect("Failed to spawn client");
	std::thread::sleep(std::time::Duration::from_secs(1));
	client1.kill().unwrap();
	client2.kill().unwrap();
	client3.kill().unwrap();
	
	server.kill().unwrap();
	assert_eq!(client1.wait().unwrap().success(), true);
	assert_eq!(client2.wait().unwrap().success(), true);
	assert_eq!(client3.wait().unwrap().success(), false);
	server.wait().unwrap();

}
