use std::io;
use std::sync::Arc;
use std::net::ToSocketAddrs;
use structopt::StructOpt;
use tokio::runtime;
use tokio::net::TcpStream;
use std::fs;
use std::path::Path;
use tokio_rustls::{ TlsConnector, rustls::ClientConfig, webpki::DNSNameRef };
use anyhow::{anyhow, Result, Context, bail};
use tokio_util::codec;
use tokio::stream::StreamExt;
use futures::SinkExt;

struct NoVerifier;

impl rustls::ServerCertVerifier for NoVerifier {
	fn verify_server_cert(
		&self,
		_roots: &rustls::RootCertStore,
		_presented_certs: &[rustls::Certificate],
		_dns_name: DNSNameRef,
		_ocsp_response: &[u8]
	) -> std::result::Result<rustls::ServerCertVerified, rustls::TLSError> {
		Ok(rustls::ServerCertVerified::assertion())
	}
}


#[derive(StructOpt)]
struct Options {
    host: String,

    /// domain
    #[structopt(short="d", long="domain")]
    domain: Option<String>,
    
     /// certificate
    #[structopt()]
    cert: String,
    
     /// certificate key
    #[structopt()]
    key: String,
    
    #[structopt(short, long, default_value = "1")]
    repeat: i64,
}

async fn do_stuff(addr: impl tokio::net::ToSocketAddrs, connector: TlsConnector, domain: String, times: i64) -> Result<()> {
	let stream = TcpStream::connect(&addr).await?;

	let domain = DNSNameRef::try_from_ascii_str(&domain)
	.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid dnsname"))?;

	let stream = connector.connect(domain, stream).await?;

	let (recv, send) = tokio::io::split(stream);
	
	let mut reader = codec::FramedRead::new(recv, codec::LengthDelimitedCodec::new());
	let mut writer = codec::FramedWrite::new(send, codec::LengthDelimitedCodec::new());

	for _ in  0..times {
		let mcall = serde_cbor::to_vec(&cborpc::MethodCall{ protocol_name: "rustorion-server-0".to_string(), method_name: "get_view".to_string(), message: vec![]}).unwrap().into();
		writer.send(mcall)
			.await
			.map_err(|e| anyhow!("failed to send request: {}", e))?;
		let a = reader.next().await.unwrap();
		let response_vec: Vec<u8> = a.unwrap().iter().cloned().collect();
		let cr: cborpc::CallResponse = serde_cbor::from_reader(response_vec.as_slice())?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		
		println!("Call made, successfully");
	};

	Ok(())
}

fn main() -> Result<()> {
	let options = Options::from_args();

	let addr = options.host
		.to_socket_addrs()?
		.next()
		.ok_or_else(|| io::Error::from(io::ErrorKind::NotFound))?;
	let domain = options.domain.unwrap_or("localhost".to_string());

	let mut runtime = runtime::Builder::new()
		.basic_scheduler()
		.enable_io()
		.build()?;
	let mut config = ClientConfig::new();
	config.dangerous().set_certificate_verifier(Arc::new(NoVerifier{}));
	
	let cert_path = Path::new(&options.cert);
	let key_path = Path::new(&options.key);
	let (cert, key) = match fs::read(&cert_path).and_then(|x| Ok((x, fs::read(&key_path)?))) {
		Ok(x) => x,
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
			println!("generating self-signed certificate");
			let mut params = rcgen::CertificateParams::new(vec!["localhost".into()]);
			params.alg = &rcgen::PKCS_ECDSA_P384_SHA384;
			let cert = rcgen::Certificate::from_params(params).unwrap();
			let key = cert.serialize_private_key_der();
			let cert = cert.serialize_der().unwrap();
			fs::write(&cert_path, &cert).context("failed to write certificate")?;
			fs::write(&key_path, &key).context("failed to write private key")?;
			(cert, key)
		}
		Err(e) => {
			bail!("failed to read certificate: {}", e);
		}
	};
	config.set_single_client_cert(vec![rustls::Certificate(cert)], rustls::PrivateKey(key))?;
	
	let connector = TlsConnector::from(Arc::new(config));

	let fut = do_stuff(addr, connector, domain, options.repeat);

	runtime.block_on(fut)
}
