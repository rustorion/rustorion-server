use std::fs;
use std::net::ToSocketAddrs;
use std::{
	sync::Arc, sync::Mutex,
};
use std::io;
use futures_util::future::TryFutureExt;
use structopt::StructOpt;
use tokio::runtime;
use tokio::net::TcpListener;
use tokio::stream::StreamExt;
use futures_util::sink::SinkExt;
use anyhow::{anyhow, bail, Context, Result};

use rustorion::universe as u;
use rustorion::storage as s;
use rustorion::universeview as uv;
use rustorion::universe::interface as ui;
use rustorion::storage::EntityStored;
use std::collections::hash_map;
use rustls::Session;

struct CustomVerifier;

impl rustls::ClientCertVerifier for CustomVerifier {

	fn client_auth_root_subjects(
		&self,
		_sni: Option<&webpki::DNSName>,
	) -> Option<rustls::DistinguishedNames> {
		return Some(vec![]);
	}
	
	fn verify_client_cert(
		&self,
		_presented_certs: &[rustls::Certificate],
		_sni: Option<&webpki::DNSName>
	) -> Result<rustls::ClientCertVerified, rustls::TLSError> {
		return Ok(rustls::ClientCertVerified::assertion());
	}
}

#[derive(StructOpt)]
struct Options {
	addr: String,
	
	#[structopt(long = "universe")]
	universe: String,
	
	/// certificate
	#[structopt()]
	cert: String,
	
	/// certificate key
	#[structopt()]
	key: String,

}

type PlayerID = blake3::Hash;

// Game state
struct State {
	universe: rustorion::universe::Universe,
	// mapping between players and empires they directly control
	player_map: bimap::BiMap<blake3::Hash, s::ID<u::Empire>>,
	turn: u16,
}

impl State {
	fn make_turn(&mut self) {
		println!("Turn {} happens!", self.turn);
		self.turn += 1;
	}
}

// Player's individual state
struct ConnectionState {
	state_rc: Arc<Mutex<State>>,
	cert_hash: PlayerID,
}


// fn load_certs(certs_data: &[u8]) -> io::Result<Vec<Certificate>> {
// 	certs(&mut BufReader::new(certs_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))
// }
// 
// fn load_keys(keys_data: &[u8]) -> io::Result<Vec<PrivateKey>> {
// 	pkcs8_private_keys(&mut BufReader::new(keys_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid key"))
// }

fn get_free_empire(state: &State) -> Option<s::ID<u::Empire>> {
	for empire in &state.universe.empires {
		let id: s::ID::<u::Empire> = s::ID::<u::Empire>::from_id(*empire.0);
		if !state.player_map.contains_right(&id) {
			return Some(id);
		}
	}
	return None;
}

async fn handle_connection(acceptor: tokio_rustls::TlsAcceptor, stream: tokio::net::TcpStream, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let stream = acceptor.accept(stream).await?;
	let certv = stream.get_ref().1.get_peer_certificates().unwrap();
	let cert_vec = certv[0].0.clone();
	let cert_hash = blake3::hash(&cert_vec);
	eprintln!("Cert incoming: {:?}", cert_hash);
	let connection_state = ConnectionState { cert_hash, state_rc: state_rc.clone() };
	let (recv, send) = tokio::io::split(stream);
	let mut reader = tokio_util::codec::FramedRead::new(recv, tokio_util::codec::LengthDelimitedCodec::new());
	let mut writer = tokio_util::codec::FramedWrite::new(send, tokio_util::codec::LengthDelimitedCodec::new());

	fn get_actions(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let state = connection_state.state_rc.lock().unwrap();
		let actor = u::action::Actor { empire_id: *state.player_map.get_by_left(&connection_state.cert_hash).ok_or(anyhow!("Empire ID not assigned yet"))? };
		let actions = &state.universe.get(actor.empire_id).ok_or(anyhow!("Bad empire ID stored"))?.submitted_actions;
		Ok(cborpc::CallResponse{success:true, message: serde_cbor::to_vec(&actions)?})
	}

	fn set_actions(connection_state: &mut ConnectionState, message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let mut state = connection_state.state_rc.lock().unwrap();
		
		let actions: Vec<u::action::Action> = serde_cbor::from_reader(message.as_slice())?;
		let actor = u::action::Actor { empire_id: *state.player_map.get_by_left(&connection_state.cert_hash).ok_or(anyhow!("Empire ID not assigned yet"))? };
		let failed_actions = u::action::Action::validate_batch(actions.as_slice(), &state.universe, &actor);
		if failed_actions.is_empty() {
			state.universe.get_mut(actor.empire_id).ok_or(anyhow!("Bad empire ID stored"))?.submitted_actions = actions;
			fn is_everyone_ready(empires: Vec<ui::Empire>) -> bool {
				empires.iter().all(|e| !e.data.submitted_actions.is_empty())
			}
			if is_everyone_ready(ui::Universe::new(&state.universe).empires()) {
				state.make_turn();
			}
			Ok(cborpc::CallResponse{success:true, message: vec![]})
		} else {
			let error_messages: Vec<String> = failed_actions.iter().map(|f| f.error.clone()).collect();
			bail!(error_messages.as_slice().join("\n"))
		}
		
	}
	
	fn get_view(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let mut state = connection_state.state_rc.lock().unwrap();
		
		// TODO: make this a separate RPC call?
		let player_empire_id = match state.player_map.get_by_left(&connection_state.cert_hash) {
			Some(e_id) => Some(*e_id),
			None => match get_free_empire(&state) {
				Some(e_id) => {
					// If this fails, we have internal inconsistency
					// TODO: get better context
					state.player_map.insert_no_overwrite(connection_state.cert_hash.clone(), e_id).map_err(|_| anyhow!("Cert hash already mapped?!"))?;
					Some(e_id)
				},
				None => None,
			},
		};
		
		match player_empire_id {
			Some(player_empire_id) => {
				let u_view = uv::Universe::empire_view(&state.universe, player_empire_id);
				Ok(cborpc::CallResponse{success: true, message: serde_cbor::to_vec(&u_view)?})
			},
			None => Ok(cborpc::CallResponse{success: false, message: serde_cbor::to_vec(&"No free empire found")?}),
		}
		
		
	};
	
	let mut responder = cborpc::Responder::new(connection_state);
	let protocols = responder.protocols();
	let mut server_protocol = hash_map::HashMap::new();
	server_protocol.insert("get_view".to_string(), get_view as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("get_actions".to_string(), get_actions as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("set_actions".to_string(), set_actions as cborpc::CallHandler<ConnectionState>);
	protocols.insert("rustorion-server-0".to_string(), server_protocol);
	
	let mut next_opt = reader.next().await;
	while next_opt.is_some() {
		let next = next_opt.ok_or(anyhow!("This will never happen"))??;
		let inpacket: &[u8] = next.as_ref(); // TODO: this one's weird
		let mut outpacket = vec![];
		let success: bool = responder.answer_call(inpacket, &mut outpacket).unwrap();
		
		writer.send(outpacket.into()).await?;
		println!("Call answered success={}", success);
		next_opt = reader.next().await;
	}

	return Ok(());
}

async fn accept_connections(addr: impl tokio::net::ToSocketAddrs, acceptor: tokio_rustls::TlsAcceptor, handle: runtime::Handle, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let mut listener = TcpListener::bind(&addr).await?;

	loop {
		let (stream, _peer_addr) = listener.accept().await?;
		let acceptor = acceptor.clone();

		let state_rc_clone = state_rc.clone();
		let fut = handle_connection(acceptor, stream, state_rc_clone);

		handle.spawn(fut.unwrap_or_else(|err| eprintln!("{:?}", err)));
	}
}

fn main() -> Result<()> {
	let options = Options::from_args();

	let addr = options.addr.to_socket_addrs()?
		.next().ok_or(anyhow!("Failed to parse host"))?;
	
	let cert_path = std::path::Path::new(&options.cert);
	let key_path = std::path::Path::new(&options.key);
	let (cert, key) = match fs::read(&cert_path).and_then(|x| Ok((x, fs::read(&key_path)?))) {
		Ok(x) => x,
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
			println!("generating self-signed certificate");
			let mut params = rcgen::CertificateParams::new(vec!["localhost".into()]);
			params.alg = &rcgen::PKCS_ECDSA_P384_SHA384;
			let cert = rcgen::Certificate::from_params(params).unwrap();
			let key = cert.serialize_private_key_der();
			let cert = cert.serialize_der().unwrap();
			fs::write(&cert_path, &cert).context("failed to write certificate")?;
			fs::write(&key_path, &key).context("failed to write private key")?;
			(cert, key)
		}
		Err(e) => {
			bail!("failed to read certificate: {}", e);
		}
	};
    
	let mut u_file = std::fs::File::open(&options.universe).unwrap();
	let mut cbor_deserializer = serde_cbor::de::Deserializer::from_reader(&mut u_file);
	let universe = serde::de::Deserialize::deserialize(&mut cbor_deserializer).unwrap();
	let state = State { universe, player_map: bimap::BiHashMap::<blake3::Hash, s::ID<u::Empire>>::new(), turn: 0 };
	let state_rc = Arc::new(Mutex::new(state));


	let mut runtime = runtime::Builder::new()
		.threaded_scheduler()
		.enable_io()
		.build()?;
	let handle = runtime.handle().clone();
	let mut config = rustls::ServerConfig::new(Arc::new(CustomVerifier{}));
	config.set_single_cert(vec![rustls::Certificate(cert)], rustls::PrivateKey(key))
		.map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err))?;
	let acceptor = tokio_rustls::TlsAcceptor::from(Arc::new(config));

	let fut = accept_connections(addr, acceptor, handle, state_rc);

	runtime.block_on(fut)
}
